# cpp_wrappers

## boost python

```
make -C boost_python
./my_script_py2.py
make -C boost_python clean
```

## swig

```
make -C swig
./my_script_py3.py
make -C swig clean
```

## pybind11

```
make -C pybind11
./my_script_py3.py
make -C pybind11 clean
```

