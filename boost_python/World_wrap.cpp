#include <World.hpp>

#include <boost/python.hpp>
using namespace boost::python;

BOOST_PYTHON_MODULE(mymodule) {

    class_<World>("World", init<>())
        .def("greet", &World::greet)
        .def("setMsg", &World::setMsg);

    class_<World2, bases<World>>("World2", init<>())
        .def("greet", &World2::greet)
        .def("greet2", &World2::greet2);

    def("add", &add);
}

