#include <World.hpp>

int add(int a, int b) {
    return a+b;
}

World::World(): msg_("nothing") {
} 

void World::setMsg(const std::string & msg) {
    msg_ = msg; 
}

std::string World::greet() const {
    return msg_; 
}

World2::World2() {
    setMsg("something");
} 

std::string World2::greet() const {
    const std::string & m = World::greet();
    return m + m;
}

std::string World2::greet2() const{
    const std::string & m = World::greet();
    return m + m + m;
}

std::map<std::string, int> getDict() {
    return {{"toto", 42}, {"tata", 37}};
}

