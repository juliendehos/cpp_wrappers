#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <World.hpp>

PYBIND11_MODULE(mymodule, m) {

    m.def("add", &add);

    pybind11::class_<World>(m, "World")
        .def(pybind11::init())
        .def("setMsg", &World::setMsg)
        .def("greet", &World::greet);

    pybind11::class_<World2, World>(m, "World2")
        .def(pybind11::init())
        .def("greet", &World2::greet)
        .def("greet2", &World2::greet2);

    m.def("getDict", &getDict);

}

