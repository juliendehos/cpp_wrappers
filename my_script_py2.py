#!/usr/bin/env python2

import mymodule

print 'test1:', 42 == mymodule.add(20, 22)

w = mymodule.World()
print 'test2:', 'nothing' == w.greet()
w.setMsg('to')
print 'test3:', 'to' == w.greet()

w2 = mymodule.World2()
print 'test4:', 'somethingsomething' == w2.greet()
w2.setMsg('to')
print 'test5:', 'toto' == w2.greet()
print 'test6:', 'tototo' == w2.greet2()

