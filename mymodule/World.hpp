#include <string>
#include <map>

int add(int, int);

class World {
    private:
        std::string msg_;
    public:
        World();
        virtual ~World() = default;
        void setMsg(const std::string & msg);
        virtual std::string greet() const;
};

class World2 : public World {
    public:
        World2();
        std::string greet() const override;
        std::string greet2() const;
};

std::map<std::string, int> getDict();

